================
Kommende Version
================

Seit 20221016 : Im Hauptmenü `DSBE` ist die Reihenfolge geändert:
:menuselection:`Art.60§7-Konventionen` steht jetzt weiter unten.  Begründung:
Menü veranschaulicht nun besser die Abhängigkeit der Funktionen voneinander.

- Klienten
- VSEs
- Stellenanbieter
- Stellen
- Stellenangebote
- Art.60§7-Konventionen
- Art.61-Konventionen
- (...)

Neue Option :setting:`jobs.with_employer_model`.


Siehe auch https://weleup.lino-framework.org/changes/coming.html
