===========
Allgemeines
===========


.. glossary::

  Lino Welfare

    Eine Sammlung :term:`Lino-Anwendungen <Lino application>` für :term:`ÖSHZ`.

  ÖSHZ

    Öffentliches Sozialhilfezentrum in Belgien.

    Ein durch die Gemeinden organisierter öffentlicher Dienst. Jede natürliche
    Person kann in einem :term:`ÖSHZ` um Hilfe bitten.

  Begünstigter

    Eine natürliche Person, die bei einem :term:`ÖSHZ` Hilfe erhält.

    Intern sagen wir statt "Begünstigter" auch manchmal "Klient".
