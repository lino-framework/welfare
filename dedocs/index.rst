.. _welfare.de:

==============
Lino für ÖSHZ
==============

Willkommen im **Benutzerhandbuch** von :term:`Lino Welfare`.

.. :term:`Lino-Anwendungen <Lino site>`
  , der freien Lino-Anwendung
  für belgische ÖSHZ.

.. Ein ähnliches Dokument existiert in `Französisch
  <https://fr.welfare.lino-framework.org/>`_.  *Technische Informationen*
  gibt es in der `englischen Version
  <https://welfare.lino-framework.org/>`_.
  Für Fragen zum *Vertrieb* und
  *professionellen Support* siehe `www.saffre-rumma.net
  <https://www.saffre-rumma.net/de/welfare/>`_.


.. toctree::
   :maxdepth: 2

   about/index
   usage/index
   changes/index


.. toctree::
   :maxdepth: 1
   :hidden:

   tour/index
