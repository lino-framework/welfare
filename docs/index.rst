.. _welfare:

============
Lino Welfare
============

Welcome to the **Lino Welfare** user guide.

This website contains technical information.
There is end-user documentation in
`German <https://de.welfare.lino-framework.org/>`_ and
`French <https://fr.welfare.lino-framework.org/>`_.
For *commercial* information please consult
`www.saffre-rumma.net <https://www.saffre-rumma.net/fr/welfare/>`_.


.. toctree::
   :maxdepth: 1

   about/index
   usage/index
   changes/index
   specs/index
   api/index
   install/index
   copyright
