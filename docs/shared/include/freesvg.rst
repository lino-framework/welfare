.. |br| raw:: html

   <br />


.. |calendar| image:: /../docs/shared/images/freesvg/1538296242.svg
    :alt: Calendar
    :height: 40
    :target: https://using.lino-framework.org/plugins/cal.html

.. |accounting| image:: /../docs/shared/images/freesvg/mono-ledger.svg
    :alt: Accounting
    :height: 40
    :target: https://using.lino-framework.org/plugins/accounting.html

.. |invoicing| image:: /../docs/shared/images/freesvg/primary-template-invoice.svg
    :alt: Invoicing
    :height: 40
    :target: https://using.lino-framework.org/plugins/invoicing.html

.. |working| image:: /../docs/shared/images/freesvg/1538296242.svg
    :alt: Work time
    :height: 40
    :target: https://using.lino-framework.org/plugins/working.html

.. |reception| image:: /../docs/shared/images/freesvg/aiga_waiting_room.svg
    :alt: Reception
    :height: 40
    :target: https://using.lino-framework.org/plugins/reception.html

.. |family| image:: /../docs/shared/images/freesvg/Family_of_Four.svg
    :alt: Family
    :height: 40
    :target: https://using.lino-framework.org/plugins/households.html

.. |social| image:: /../docs/shared/images/freesvg/Social-service-icon.svg
    :alt: Social service
    :height: 40

.. |presentation| image:: /../docs/shared/images/freesvg/primary-scheme-presentation.svg
    :alt: Presentation
    :height: 40

.. |handshake| image:: /../docs/shared/images/freesvg/1520519560.svg
    :alt: Handshake
    :height: 40
    :target: https://using.lino-framework.org/plugins/contacts.html

.. |busy| image:: /../docs/shared/images/freesvg/busy.svg
    :alt: Busy
    :height: 40
