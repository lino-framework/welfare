.. _ug.plugins.art60:

=====================================
``art60`` :  Art 60§7 job supplyments
=====================================

Technical docs in :ref:`welfare.plugins.art60`.


.. contents::
   :local:
   :depth: 1


Glossary
========

.. glossary::

  article 60 job supplyment

    (French: *Mise au travail en application de l'article 60§7*).

    A :term:`job supplyment` regulated by article 60.

    Represented in the database by rows of :class:`art60.Contract
    <lino_welfare.modlib.art60.Contract>`.
