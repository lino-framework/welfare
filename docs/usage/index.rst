=====
Usage
=====


.. toctree::
   :maxdepth: 1

   art60
   art61
   testing
   jobs
   debts


Plugins reference
=================


.. _ug.plugins.newcomers:
.. _ug.plugins.pcsw:
.. _ug.plugins.isip:
.. _ug.plugins.integ:
.. _ug.plugins.immersion:
.. _ug.plugins.esf:
.. _ug.plugins.clients:
.. _ug.plugins.cbss:
.. _ug.plugins.aids:
.. _ug.plugins.active_job_search:
.. _ug.plugins.badges:
.. _ug.plugins.xcourses:
.. _ug.plugins.b2c:
.. _ug.plugins.dupable_clients:

Not yet written
---------------

Not yet written
