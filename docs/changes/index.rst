.. _weleup.changes:
.. _welcht.changes:
.. _welfare.changes:

=======
Changes
=======

Since 2021 we log changes in Welfare in the
`general Lino changelog <https://www.lino-framework.org/changes>`__


.. toctree::
   :maxdepth: 1

   2020
   20200722
   2019
