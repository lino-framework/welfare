.. _welfare.install:

============
Installation
============

To install a Lino Welfare, use `getlino
<https://lino-framework.gitlab.io/getlino>`__ and select either ``weleup`` or
``welcht`` as ``APPNAME``.
