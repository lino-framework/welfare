=======
Plugins
=======

.. toctree::
    :maxdepth: 1

    clients
    contacts
    coachings
    users
    pcsw
    welfare
    newcomers
    households
    dupable_clients
    checkdata
    aids/index
    cbss
    xcourses
    debts
    excerpts
    isip
    jobs
    reception/index
    uploads
    addresses
    cal
    countries
    notes
    notify
    memo
    printing
    b2c
    accounting
    vatless
    finan
    esf
    art60
    art61
