.. doctest docs/specs/contacts.rst
.. _welfare.plugins.contacts:

================================================
`contacts` : The contacts plugin in Lino Welfare
================================================

.. currentmodule:: lino_welfare.modlib.contacts

The :mod:`lino_welfare.modlib.contacts` plugin extends the
:mod:`lino_xl.lib.contacts` plugin

This document assumes that you have read :ref:`ug.plugins.contacts`.

.. contents::
   :local:
   :depth: 1


Class reference
===============

.. class:: Company

  .. attribute:: job_provider

    The :term:`job provider` for whom this company acts as a workplace.

    See :ref:`art60_workplace`.
