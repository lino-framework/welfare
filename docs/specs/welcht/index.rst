.. _welcht.specs:

===========================
Lino Welfare Chatelet Specs
===========================

This section describes of what you can do with :ref:`welcht`.


.. toctree::
    :maxdepth: 2

    clients_chatelet
    chatelet
    immersion
    cv2
    courses
    polls
    misc
    integ
    isip_chatelet
    /apps/welcht/2024
