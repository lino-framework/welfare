=======
General
=======


.. glossary::

  Lino Welfare

    A collection of :term:`Lino applications <Lino application>` for
    :term:`PCSW`\ s.

  PCSW

    A Public Centre for Social Welfare in Belgium.

    A service organized by every municipality in Belgium. Every natural person
    in need can ask for help in a PCSW.

  beneficiary

    A natural person who receives some form of social help from a :term:`PCSW`.

    We sometimes use the word "client" to refer to a :term:`beneficiary`.

  social agent

    An employee of a :term:`PCSW`.

  coaching

    The fact that a given :term:`social agent` is or was the responsible contact
    person for a given :term:`beneficiary` during a given date range.
