============================
The ``lino-welfare`` package
============================



Lino Welfare is a
`Lino <https://www.lino-framework.org>`__
plugin library
for Belgian
*Public Centres for Social Welfare*.

- Project homepage: https://gitlab.com/lino-framework/welfare

- Documentation: https://welfare.lino-framework.org

- There are *user guides* in `French
  <https://welfare.lino-framework.org/fr>`_ and `German
  <https://welfare.lino-framework.org/de>`_.

- There are two applications using this library:
  `Châtelet <https://gitlab.com/lino-framework/welcht>`__
  and `Eupen <https://gitlab.com/lino-framework/weleup>`__

- Online demo sites at
  https://welcht1.mylino.net
  and
  https://weleup1.mylino.net

- This is an integral part of the Lino framework, which is documented
  at https://www.lino-framework.org

- Changelog: https://www.lino-framework.org/changes

- For introductions, commercial information and hosting solutions
  see https://www.saffre-rumma.net

- This is a sustainably free open-source project. Your contributions are
  welcome.  See https://community.lino-framework.org for details.



