# -*- coding: UTF-8 -*-
# Copyright 2014-2021 Rumma & Ko Ltd
"""Main settings module for `lino_welfare.projects.gerd`.

.. autosummary::
   :toctree:

   demo
   doctests
   memory
   mysql

"""

from lino_weleup.settings import *

# the following line should not be active in a checked-in version
# DATABASES['default']['NAME'] = ':memory:'
