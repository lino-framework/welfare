# -*- coding: UTF-8 -*-
# Copyright 2002-2020 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)
"""
This package defines functionality specific to :ref:`welfare`.

.. autosummary::
   :toctree:

   modlib
   migrate


"""

from .setup_info import SETUP_INFO

# doc_trees = ['docs', 'dedocs', 'frdocs']
# doc_trees = ['dedocs', 'frdocs']
# doc_trees = ['docs']
intersphinx_urls = dict(docs="https://welfare.lino-framework.org",
                        dedocs="https://welfare.lino-framework.org/de",
                        frdocs="https://welfare.lino-framework.org/fr")
srcref_url = 'https://gitlab.com/lino-framework/welfare/blob/master/%s'
