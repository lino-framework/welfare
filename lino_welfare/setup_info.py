# -*- coding: UTF-8 -*-
# Copyright 2002-2024 Rumma & Ko Ltd
# License: GNU Affero General Public License v3 (see file COPYING for details)

# This module is part of the Lino Welfare test suite.
# To test only this module:
#   $ python setup.py test -s tests.PackagesTests

requires = ['lino-cosi', 'pytidylib', 'django-iban', 'metafone', 'cairocffi']
requires.append('suds-py3')

SETUP_INFO = dict(name='lino-welfare',
                  version='24.5.0',
                  install_requires=requires,
                  extras_require={
                      'testing': ['pytest', 'pytest-html', 'pytest-forked', 'pytest-env']
                  },
                  include_package_data=True,
                  zip_safe=False,
                  description="A Lino plugin library for Belgian PCSWs",
                  long_description="""\
Lino Welfare is a
`Lino <https://www.lino-framework.org>`__
plugin library
for Belgian
*Public Centres for Social Welfare*.

- Project homepage: https://gitlab.com/lino-framework/welfare

- Documentation: https://welfare.lino-framework.org

- There are *user guides* in `French
  <https://welfare.lino-framework.org/fr>`_ and `German
  <https://welfare.lino-framework.org/de>`_.

- There are two applications using this library:
  `Châtelet <https://gitlab.com/lino-framework/welcht>`__
  and `Eupen <https://gitlab.com/lino-framework/weleup>`__

- Online demo sites at
  https://welcht1.mylino.net
  and
  https://weleup1.mylino.net

- This is an integral part of the Lino framework, which is documented
  at https://www.lino-framework.org

- Changelog: https://www.lino-framework.org/changes

- For introductions, commercial information and hosting solutions
  see https://www.saffre-rumma.net

- This is a sustainably free open-source project. Your contributions are
  welcome.  See https://community.lino-framework.org for details.


""",
                  author='Rumma & Ko Ltd',
                  author_email='info@lino-framework.org',
                  url="https://gitlab.com/lino-framework/welfare",
                  license_files=['COPYING'],
                  classifiers="""\
Programming Language :: Python
Programming Language :: Python :: 3
Development Status :: 5 - Production/Stable
Environment :: Web Environment
Framework :: Django
Intended Audience :: Developers
Intended Audience :: System Administrators
License :: OSI Approved :: GNU Affero General Public License v3
Natural Language :: English
Natural Language :: French
Natural Language :: German
Operating System :: OS Independent
Topic :: Database :: Front-Ends
Topic :: Home Automation
Topic :: Office/Business
Topic :: Sociology :: Genealogy
Topic :: Education""".splitlines())

SETUP_INFO.update(extras_require={
    'testing': ['pytest', 'pytest-html', 'pytest-forked', 'pytest-env']
})

SETUP_INFO.update(packages=[
    'lino_welfare',
    'lino_welfare.modlib',
    'lino_welfare.modlib.active_job_search',
    'lino_welfare.modlib.active_job_search.fixtures',
    'lino_welfare.modlib.aids',
    'lino_welfare.modlib.aids.fixtures',
    'lino_welfare.modlib.art61',
    'lino_welfare.modlib.art61.fixtures',
    'lino_welfare.modlib.badges',
    'lino_welfare.modlib.cal',
    'lino_welfare.modlib.cal.fixtures',
    'lino_welfare.modlib.cbss',
    'lino_welfare.modlib.cbss.fixtures',
    'lino_welfare.modlib.cbss.management',
    'lino_welfare.modlib.cbss.management.commands',
    'lino_welfare.modlib.client_vouchers',
    'lino_welfare.modlib.contacts',
    'lino_welfare.modlib.contacts.fixtures',
    'lino_welfare.modlib.contacts.management',
    'lino_welfare.modlib.contacts.management.commands',
    'lino_welfare.modlib.xcourses',
    'lino_welfare.modlib.xcourses.fixtures',
    'lino_welfare.modlib.cv',
    'lino_welfare.modlib.cv.fixtures',
    'lino_welfare.modlib.debts',
    'lino_welfare.modlib.debts.fixtures',
    'lino_welfare.modlib.dupable_clients',
    'lino_welfare.modlib.dupable_clients.fixtures',
    'lino_welfare.modlib.finan',
    'lino_welfare.modlib.finan.fixtures',
    'lino_welfare.modlib.esf',
    'lino_welfare.modlib.esf.fixtures',
    'lino_welfare.modlib.households',
    'lino_welfare.modlib.households.fixtures',
    'lino_welfare.modlib.integ',
    'lino_welfare.modlib.integ.fixtures',
    'lino_welfare.modlib.isip',
    'lino_welfare.modlib.jobs',
    'lino_welfare.modlib.jobs.fixtures',
    'lino_welfare.modlib.accounting',
    'lino_welfare.modlib.accounting.fixtures',
    'lino_welfare.modlib.notes',
    'lino_welfare.modlib.notes.fixtures',
    'lino_welfare.modlib.newcomers',
    'lino_welfare.modlib.newcomers.fixtures',
    'lino_welfare.modlib.pcsw',
    'lino_welfare.modlib.pcsw.fixtures',
    'lino_welfare.modlib.polls',
    'lino_welfare.modlib.polls.fixtures',
    'lino_welfare.modlib.projects',
    'lino_welfare.modlib.reception',
    'lino_welfare.modlib.trading',
    'lino_welfare.modlib.sepa',
    'lino_welfare.modlib.sepa.fixtures',
    'lino_welfare.modlib.system',
    'lino_welfare.modlib.immersion',
    'lino_welfare.modlib.immersion.fixtures',
    'lino_welfare.modlib.users',
    'lino_welfare.modlib.users.fixtures',
    'lino_welfare.modlib.welfare',
    'lino_welfare.modlib.welfare.fixtures',
    'lino_welfare.modlib.welfare.management',
    'lino_welfare.modlib.welfare.management.commands',
    'lino_welfare.scripts',
    'lino_welfare.projects',
    'lino_welfare.projects.gerd',
    'lino_welfare.projects.gerd.settings',
    'lino_welfare.projects.gerd.tests',
    'lino_welfare.projects.mathieu',
    'lino_welfare.projects.mathieu.settings',
    'lino_welfare.projects.mathieu.tests',
])

SETUP_INFO.update(include_package_data=True)
