.. _welfare.fr:

==============
Lino pour CPAS
==============

Bienvenue sur le **site de documentation en français** de Lino Welfare.

Un site similaire existe en `allemand
<https://de.welfare.lino-framework.org/>`_.
Pour des informations *commerciales* veuillez consulter
`www.saffre-rumma.net <https://www.saffre-rumma.net/fr/welfare/>`_.
Pour des informations
*techniques* veuillez consulter la version `anglaise
<https://welfare.lino-framework.org/>`_.

Documents imprimables:

- `Tutoriel Lino </dl/Tutoriel_Lino.pdf>`__
  maintenu par les centres qui utilisent Lino.
- `Présentation Lino Welfare </dl/tour_lino_welfare.pdf>`__



.. Il y avait également un `Wiki
    <http://trac.lino-framework.org/wiki/Welfare/FR>`_ qui ne semble
    pas être la bonne formule et nous comptons le supprimer à moyen
    terme.
  `Fiche technique Lino Welfare <dl/fiche_lino_welfare.pdf>`__ mainten


Table des matières
==================

.. toctree::
   :maxdepth: 2

   /tour/index
   clients
   users
   jobs
   /glossaire
   /help/index
   /changes/index



.. toctree::
   :maxdepth: 1
   :hidden:

   /contribuer
   Fiche technique <fiche_technique>
   /todo
